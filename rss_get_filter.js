var Discord = require('discord.js');
var logger = require('winston');
var auth = require('./auth.json');
const Parser = require('rss-parser');
const _ = require('lodash');
var cron = require('node-cron');

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';
// Initialize Discord Bot
const bot = new Discord.Client();

bot.on('ready', function (evt) {
    logger.info('Connected');
	seekRss();	
	cron.schedule('0 */2 * * *', () => seekRss());
});


const FEED = 'https://.../rss.xml';
var titles =[];
const SEEK = [
	'...',
];
let parser = new Parser();
function seekRss() {
	parser.parseURL(FEED).then(feed => {
		_.forEach(feed.items, element => {
			let title = element.title;
			if(!_.includes(titles, title) && _.includes(SEEK, title))
			{
				titles.push(title);
				let canal = bot.channels.get('706862570455433226'); //seek
				sendTitleAsync(canal, title);
			}
		});
	});
}

async function sendTitleAsync (canal, msg) {
	await canal.send(msg).catch('Error while posting title');
}